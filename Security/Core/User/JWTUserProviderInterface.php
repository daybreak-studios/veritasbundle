<?php
	namespace DaybreakStudios\VeritasBundle\Security\Core\User;

	use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
	use Symfony\Component\Security\Core\User\UserInterface;
	use Symfony\Component\Security\Core\User\UserProviderInterface;

	interface JWTUserProviderInterface extends UserProviderInterface {
		/**
		 * @param TokenInterface $token
		 *
		 * @return UserInterface
		 */
		public function loadUserByToken(TokenInterface $token);
	}