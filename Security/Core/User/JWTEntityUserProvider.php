<?php
	namespace DaybreakStudios\VeritasBundle\Security\Core\User;

	use Doctrine\Bundle\DoctrineBundle\Registry;
	use Doctrine\ORM\EntityManagerInterface;
	use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
	use Symfony\Component\Security\Core\Exception\AuthenticationException;
	use Symfony\Component\Security\Core\User\UserInterface;

	class JWTEntityUserProvider implements JWTUserProviderInterface {
		/**
		 * @var EntityManagerInterface
		 */
		protected $em;

		/**
		 * @var string
		 */
		protected $userClass;

		/**
		 * @var string
		 */
		protected $userAlias;

		/**
		 * @var string
		 */
		protected $property;

		/**
		 * JWTEntityUserProvider constructor.
		 *
		 * @param Registry $doctrine
		 * @param string   $entityName
		 * @param string   $property
		 */
		public function __construct(Registry $doctrine, $entityName, $property = 'id') {
			$this->em = $doctrine->getManager();
			$this->property = $property;

			$this->userAlias = $entityName;

			if (strpos($entityName, ':') !== false)
				$this->userClass = $this->em->getClassMetadata($entityName)->getName();
			else
				$this->userClass = $entityName;
		}

		/**
		 * @return EntityManagerInterface
		 */
		protected function getEntityManager() {
			return $this->em;
		}

		/**
		 * @return string
		 */
		public function getUserClass() {
			return $this->userClass;
		}

		/**
		 * @return string
		 */
		public function getUserAlias() {
			return $this->userAlias;
		}

		/**
		 * @return string
		 */
		public function getProperty() {
			return $this->property;
		}

		public function loadUserByToken(TokenInterface $token) {
			$user = $this->getEntityManager()->getRepository($this->getUserAlias())->findOneBy([
				$this->getProperty() => $token->getUsername(),
			]);

			if ($user === null)
				throw new AuthenticationException('Could not find user by token');

			return $user;
		}

		public function loadUserByUsername($username) {
			throw new \BadMethodCallException('This user provider does not support loading by username');
		}

		public function refreshUser(UserInterface $user) {
			throw new \BadMethodCallException('This user provider does not support refreshing users');
		}

		public function supportsClass($class) {
			return is_a($class, $this->getUserClass(), true);
		}
	}