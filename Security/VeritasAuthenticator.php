<?php
	namespace DaybreakStudios\VeritasBundle\Security;

	use DaybreakStudios\Veritas\Exception\BadTokenException;
	use DaybreakStudios\VeritasBundle\Security\Core\User\JWTUserProviderInterface;
	use DaybreakStudios\VeritasBundle\Service\VeritasService;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
	use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
	use Symfony\Component\Security\Core\Exception\AuthenticationException;
	use Symfony\Component\Security\Core\Exception\BadCredentialsException;
	use Symfony\Component\Security\Core\User\UserProviderInterface;
	use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
	use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

	class VeritasAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface {
		/**
		 * @var VeritasService
		 */
		protected $veritas;

		/**
		 * VeritasAuthenticator constructor.
		 *
		 * @param VeritasService $veritas
		 * @param array          $audience An (optional) array of audience claims supported by the authenticator
		 */
		public function __construct(VeritasService $veritas, array $audience = []) {
			$this->veritas = $veritas;
		}

		/**
		 * @return VeritasService
		 */
		protected function getVeritasService() {
			return $this->veritas;
		}

		/**
		 * {@inheritdoc}
		 */
		public function createToken(Request $request, $providerKey) {
			$auth = $request->headers->get('Authorization');

			if ($auth === null)
				return new PreAuthenticatedToken('anon.', null, $providerKey);

			$type = strtok($auth, ' ');

			if (strcasecmp($type, 'Bearer') !== 0)
				return new PreAuthenticatedToken('anon.', null, $providerKey);

			$token = strtok('');

			try {
				$token = $this->getVeritasService()->parse($token);
			} catch (BadTokenException $e) {
				throw new BadCredentialsException('Invalid token');
			}

			return new PreAuthenticatedToken((string)$token->getSubject(), $token, $providerKey);
		}

		/**
		 * {@inheritdoc}
		 */
		public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey) {
			if (!($userProvider instanceof JWTUserProviderInterface))
				throw new \InvalidArgumentException('Veritas only supports the ' . JWTUserProviderInterface::class .
					' user provider');

			$user = $userProvider->loadUserByToken($token);

			if (!$user)
				throw new AuthenticationException('Invalid token');

			return new PreAuthenticatedToken($user, $token, $providerKey, $user->getRoles());
		}

		/**
		 * {@inheritdoc}
		 */
		public function supportsToken(TokenInterface $token, $providerKey) {
			return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
		}

		/**
		 * {@inheritdoc}
		 */
		public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
			return new Response('Authentication failed: ' . $exception->getMessage(), Response::HTTP_FORBIDDEN);
		}
	}