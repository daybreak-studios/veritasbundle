<?php
	namespace DaybreakStudios\VeritasBundle\Controller;

	use DaybreakStudios\Veritas\Exception\BadTokenException;
	use DaybreakStudios\VeritasBundle\Event\TokenEvents;
	use DaybreakStudios\VeritasBundle\Event\TokenGenerateEvent;
	use DaybreakStudios\VeritasBundle\Security\Core\User\VeritasUserInterface;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\Security\Core\User\UserInterface;

	class AuthenticationController extends Controller {
		/**
		 * @param Request $request
		 * @param string  $repository  The Doctrine repository to load the user from
		 * @param string  $property    The entity field that holds the user's username
		 * @param string  $usernameKey The payload key that contains the user's username
		 * @param string  $passwordKey The payload key that contains the user's password
		 * @param array   $audience    Any values to place into the "audience" claim
		 *
		 * @return JsonResponse
		 */
		public function authAction(Request $request, $repository, $property, $usernameKey = 'username', $passwordKey = 'password', $audience = []) {
			$payload = new ParameterBag(json_decode($request->getContent(), true) ?: []);

			/** @var VeritasUserInterface|UserInterface|null $user */
			$user = $this->getDoctrine()->getRepository($repository)->findOneBy([
				$property => $payload->get($usernameKey),
			]);

			if (!$user)
				return $this->respond($request, [
					'error' => 'Credentials not found',
				], Response::HTTP_FORBIDDEN);
			else if (!($user instanceof VeritasUserInterface))
				throw new \UnexpectedValueException('Users authenticated by Veritas must implement ' .
					VeritasUserInterface::class);

			if (!$this->get('security.password_encoder')->isPasswordValid($user, $payload->get($passwordKey)))
				return $this->respond($request, [
					'error' => 'Credentials not found',
				], Response::HTTP_FORBIDDEN);

			$veritas = $this->get('dbstudios_veritas.service.veritas');
			$builder = $veritas->build()
				->subject($user->getSubjectIdentifier())
				->audience($audience);

			$this->get('event_dispatcher')->dispatch(TokenEvents::GENERATE, new TokenGenerateEvent($builder));

			return $this->respond($request, [
				'token' => $builder->issue(),
			]);
		}

		/**
		 * @param Request $request
		 *
		 * @return JsonResponse
		 */
		public function refreshAction(Request $request) {
			$auth = $request->headers->get('Authorization');

			if (!$auth)
				return $this->respond($request, [
					'error' => 'You must provide a valid token in Authorization header to receive a new token',
				]);

			$type = strtok($auth, ' ');

			if (strcasecmp($type, 'bearer') !== 0)
				return $this->respond($request, [
					'error' => 'You must provide a valid token in Authorization header to receive a new token',
				]);

			$token = strtok('');
			$veritas = $this->get('dbstudios_veritas.service.veritas');

			try {
				$token = $veritas->refresh($token);
			} catch (BadTokenException $e) {
				return $this->respond($request, [
					'error' => 'Could not issue new token; ' . $e->getMessage(),
				]);
			}

			return $this->respond($request, [
				'token' => $token,
			]);
		}

		/**
		 * @param Request $request
		 * @param array   $data
		 * @param int     $status
		 * @param array   $headers
		 *
		 * @return JsonResponse
		 */
		private function respond(Request $request, array $data, $status = Response::HTTP_OK, array $headers = []) {
			$resp = new JsonResponse($data, $status, $headers);

			if ($cb = $request->get('callback'))
				$resp->setCallback($cb);

			return $resp;
		}
	}