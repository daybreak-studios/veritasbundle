<?php
	namespace DaybreakStudios\VeritasBundle\DependencyInjection;

	use DaybreakStudios\Veritas\Signers\Sha256HashHMACSigner;
	use DaybreakStudios\VeritasBundle\Service\VeritasService;
	use Symfony\Component\Config\Definition\Builder\TreeBuilder;
	use Symfony\Component\Config\Definition\ConfigurationInterface;

	/**
	 * This is the class that validates and merges configuration from your app/config files.
	 *
	 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
	 */
	class Configuration implements ConfigurationInterface {
		/**
		 * {@inheritdoc}
		 */
		public function getConfigTreeBuilder() {
			$treeBuilder = new TreeBuilder();
			$rootNode = $treeBuilder->root('dbstudios_veritas');

			$rootNode->children()
				->scalarNode('secret')
					->cannotBeEmpty()
					->defaultValue(VeritasService::DEFAULT_SECRET);

			$rootNode->children()
				->scalarNode('signer')
					->cannotBeEmpty()
					->defaultValue(Sha256HashHMACSigner::class);

			$rootNode->children()
				->integerNode('time_to_live')
					->defaultValue(604800); // 1 week

			return $treeBuilder;
		}
	}
