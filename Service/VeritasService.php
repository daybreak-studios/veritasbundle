<?php
	namespace DaybreakStudios\VeritasBundle\Service;

	use DaybreakStudios\Veritas\Token\TokenInterface;
	use DaybreakStudios\Veritas\Veritas;

	class VeritasService {
		const DEFAULT_SECRET = '__thisisnotsecretchangeit__';

		/**
		 * @var Veritas|null
		 */
		private $veritas = null;

		public function init($secret, $ttl, $signerClass) {
			if ($secret === self::DEFAULT_SECRET)
				throw new \Exception('You must set the secret in order to use Veritas!');

			$this->veritas = new Veritas(new $signerClass(), $secret, $ttl);
		}

		public function isInitialized() {
			return $this->veritas !== null;
		}

		public function build() {
			$this->initCheck();

			return $this->veritas->build();
		}

		/**
		 * @param string   $subject
		 * @param array    $data
		 * @param int|null $ttl
		 *
		 * @return string
		 */
		public function issue($subject, array $data = [], $ttl = null) {
			$this->initCheck();

			return $this->veritas->issue($data, $subject, $ttl);
		}

		/**
		 * @param string   $token
		 * @param array    $data
		 * @param int|null $ttl
		 *
		 * @return string
		 */
		public function refresh($token, array $data = [], $ttl = null) {
			$this->initCheck();

			return $this->veritas->refresh($token, $data, $ttl);
		}

		/**
		 * @param string $token
		 *
		 * @return TokenInterface
		 * @throws \DaybreakStudios\Veritas\Exception\TokenExpiredException
		 * @throws \DaybreakStudios\Veritas\Exception\TokenInvalidException
		 * @throws \DaybreakStudios\Veritas\Exception\TokenNotYetValidException
		 */
		public function parse($token) {
			$this->initCheck();

			return $this->veritas->parse($token);
		}

		private function initCheck() {
			if (!$this->isInitialized())
				throw new \Exception('Veritas has not yet been initialized by Symfony');
		}
	}