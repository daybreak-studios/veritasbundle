<?php
	namespace DaybreakStudios\VeritasBundle\Listener;

	use DaybreakStudios\Veritas\Claims;
	use DaybreakStudios\VeritasBundle\Event\TokenGenerateEvent;

	class TokenSetAudienceListener extends TokenSetClaimListener {
		/**
		 * @var bool
		 */
		private $append;

		/**
		 * TokenSetAudienceListener constructor.
		 *
		 * @param array $audience
		 * @param bool  $append
		 */
		public function __construct(array $audience, $append = false) {
			parent::__construct(Claims::AUDIENCE, $audience);

			$this->append = $append;
		}

		public function onTokenGenerate(TokenGenerateEvent $event) {
			$builder = $event->getBuilder();
			$audience = $this->getValue();

			if ($this->append && $current = $builder->getAudience())
				$audience = array_merge($current, $audience);

			$builder->audience($audience);
		}
	}