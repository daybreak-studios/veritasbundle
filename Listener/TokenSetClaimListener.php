<?php
	namespace DaybreakStudios\VeritasBundle\Listener;

	use DaybreakStudios\VeritasBundle\Event\TokenGenerateEvent;

	class TokenSetClaimListener {
		/**
		 * @var string
		 */
		private $claim;

		/**
		 * @var string
		 */
		private $value;

		/**
		 * TokenSetClaimListener constructor.
		 *
		 * @param string $claim
		 * @param string $value
		 */
		public function __construct($claim, $value) {
			$verb = substr($claim, 0, 3);

			if ($claim === 'issue' || in_array($verb, ['get', 'set']))
				throw new \InvalidArgumentException($claim . ' is not a valid JWT claim');

			$this->claim = $claim;
			$this->value = $value;
		}

		/**
		 * @return string
		 */
		public function getClaim() {
			return $this->claim;
		}

		/**
		 * @return string
		 */
		public function getValue() {
			return $this->value;
		}

		/**
		 * @param TokenGenerateEvent $event
		 */
		public function onTokenGenerate(TokenGenerateEvent $event) {
			$event->getBuilder()->set($this->getClaim(), $this->getValue());
		}
	}