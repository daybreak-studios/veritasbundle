<?php
	namespace DaybreakStudios\VeritasBundle\Event;

	use DaybreakStudios\Veritas\TokenBuilder\TokenBuilderInterface;
	use Symfony\Component\EventDispatcher\Event;

	class TokenGenerateEvent extends Event {
		/**
		 * @var TokenBuilderInterface
		 */
		private $builder;

		/**
		 * TokenGenerateEvent constructor.
		 *
		 * @param TokenBuilderInterface $builder
		 */
		public function __construct(TokenBuilderInterface $builder) {
			$this->builder = $builder;
		}

		/**
		 * @return TokenBuilderInterface
		 */
		public function getBuilder() {
			return $this->builder;
		}

		/**
		 * @param TokenBuilderInterface $builder
		 *
		 * @return $this
		 */
		public function setBuilder(TokenBuilderInterface $builder) {
			$this->builder = $builder;

			return $this;
		}
	}