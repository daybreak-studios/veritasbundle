<?php
	namespace DaybreakStudios\VeritasBundle\Event;

	class TokenEvents {
		const GENERATE = 'veritas.token.generate';
	}