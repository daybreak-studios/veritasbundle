<?php
	namespace DaybreakStudios\VeritasBundle;

	use DaybreakStudios\VeritasBundle\DependencyInjection\DaybreakStudiosVeritasExtension;
	use Symfony\Component\HttpKernel\Bundle\Bundle;

	class DaybreakStudiosVeritasBundle extends Bundle {
		public function getContainerExtension() {
			return new DaybreakStudiosVeritasExtension();
		}
	}
